FROM node:12.18.0-alpine3.9
RUN mkdir build
WORKDIR /build
COPY /package.json .
RUN npm i
RUN echo "npm packges installed"
COPY /src ./scr
COPY /config ./config
COPY /tsconfig.json .
RUN echo "BEFORE Compilation" 
RUN ls
RUN npm run publish
RUN echo "Source Code Dropped to Docker"
RUN echo "BEFORE DELETE "
ENV NODE_ENV='production'
RUN ls 
RUN rm -rf ./src
RUN echo "AFTER DELETE" 
RUN ls 
RUN echo  "Image Formed"
EXPOSE 3000
CMD ["npm","run","start"] 