import * as MainRoute from './plugins/Main/route'
import * as ServeData from './plugins/Servedata/route'
export class Manifest {
    public plugins = [
        new MainRoute.default().routeData,
        new ServeData.default().routeData
    ]
}