const Config = require('config');
import { createLogger, format, transports } from 'winston';
import winston = require('winston');
export class Logger {
    public static _instance: Logger;
    private writer: any
    constructor() {
        this.writer = createLogger({
            levels: this.configureLogLevels(),
            level: Config['CONSOLE_LOG_LEVEL'],
            format: format.combine(
                format.json(),
                format.splat(),
                format.timestamp(),
                format.prettyPrint()
            ),
            transports: [new transports.Console()]
        });
    }
    public static get Instance() {
        if (!this._instance) {
            console.log("Instance initialised");
            this._instance = new Logger();
        }
        return this._instance
    }
    configureLogLevels() {
        return {
            error: 0,
            warn: 1,
            info: 2,
            debug: 3
        }
    }
    public debug(message: string, ...logdata: any) {
        Logger._instance.writer.debug(message, logdata);
    }
    public info(message: string, ...logdata: any) {
        Logger._instance.writer.debug(message, logdata);
    }
    public warn(message: string, ...logdata: any) {
        Logger._instance.writer.debug(message, logdata);
    }
    public error(message: string, ...logdata: any) {
        Logger._instance.writer.debug(message, logdata);
    }
}
