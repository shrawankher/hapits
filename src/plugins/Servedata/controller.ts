import * as Hapi from "@hapi/hapi"
import { Logger } from "../../utils/logger"
import {DataServeService} from "./service"
export class DataServeController {
     getHugeData() {
        return async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
            Logger.Instance.info('Request recieved to get huge data');
            let result = await new DataServeService().getHugeData()
            Logger.Instance.info('Request completed to get huge data');
            return result;
        }
    }
}