import * as Hapi from "@hapi/hapi"
import { DataServeController } from './controller'
export default class Route {
    private DataService: DataServeController = new DataServeController();
    public routeData = {
        prefix: '/hugedata',
        name: 'GetHugeData',
        routeCollection: () => {
            return async (Server: Hapi.Server, Options: Hapi.RouteOptions) => {
                Server.route([
                    {
                        method: 'GET',
                        path: '/test',
                        handler: this.DataService.getHugeData()
                    }
                ]);
            }
        }
    }
}