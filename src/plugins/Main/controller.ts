import * as Hapi from "@hapi/hapi"
import * as Os from 'os'
const serviceInfo = require('../../../package.json')
export class MainController {
     getServiceDetails() {
        return async (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
            return {
                ServiceName:serviceInfo.name,
                Version:serviceInfo.version,
                OsType:Os.type(),
                OSName:Os.hostname(),
                CPUs:Os.cpus(),
                MemoryInBytes:Os.totalmem()
            };
        }
    }
}