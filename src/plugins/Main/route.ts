import * as Hapi from "@hapi/hapi"
import { MainController } from './controller'
export default class Route {
    private mainController: MainController = new MainController();
    public routeData = {
        prefix: '/serviceInfo',
        name: 'ServiceInfo',
        routeCollection: () => {
            return async (Server: Hapi.Server, Options: Hapi.RouteOptions) => {
                Server.route([
                    {
                        method: 'GET',
                        path: '/',
                        handler: this.mainController.getServiceDetails()
                    }
                ]);
            }
        }
    }
}