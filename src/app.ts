import * as Hapi from "@hapi/hapi"
const service = require('../package.json')
const config = require('config');
import { Manifest } from './manifest'
import { Logger } from './utils/logger'
/**
 *
 *
 * @class StartUpServer
 */
class StartUpServer {

    /**
     *
     *
     * @memberof StartUpServer
     */

    async init() {
        try {
            await this.setEnvironmentVariable();
            let Server = new Hapi.Server({
                host: config["HOST"],
                port: config["PORT"]
            });
            await this.registerPlugins(Server);
            await Server.start();
            console.log(`============:Server started:============\n\n     Service     :     ${service.name}\n     Host        :     ${Server.info.host}\n     Port        :     ${Server.info.port}\n\n=======================================`)
        } catch (error) {
            throw error;
        }
    }

    /**
     *
     *
     * @memberof StartUpServer
     */
    async setEnvironmentVariable() {
        let varPrefix = config["ENVPREFIX"];
        console.log(varPrefix)
        let environmentvariables = process.env;
        let envKeys = Object.keys(environmentvariables);
        console.log('Loading environment variables');
        envKeys.forEach(key => {
            // filter for enviroment variables
            if (key.startsWith(varPrefix)) {
                config[key] = environmentvariables[key]
                console.log(key);
            }
        });
    }

    /**
     *
     *
     * @param {Hapi.Server} Server
     * @memberof StartUpServer
     */
    async registerPlugins(Server: Hapi.Server) {
        Logger.Instance.info('Attempting to register all the plugins')
        let ManifestList: Array<any> = new Manifest().plugins;
        Logger.Instance.info('Total number of available plugins', { avaialblePlugins: ManifestList.length })
        try {
            for (let pluginIndex = 0; pluginIndex < ManifestList.length; pluginIndex++) {
                let routeData = ManifestList[pluginIndex];
                Logger.Instance.info(`Attempting to register plugin`, { Name: routeData.name });
                await Server.register({
                    name: routeData.name,
                    register: routeData.routeCollection(),
                }, {
                    routes: {
                        prefix: routeData.prefix
                    }
                });
                Logger.Instance.info(`Successfully registered  Plugin`, { Name: routeData.name });
            }
            Logger.Instance.info('Successfully registered all plugins');
        }
        catch (error) {
            Logger.Instance.error('Error while registering pluins', { error: error });
            throw error;
        }
    }
}

if (!module.parent) {
    try {
        let startApp = new StartUpServer();
        startApp.init()
    }
    catch (error) {
        console.log('Unfortunately server crashed');
        console.log(error);
        process.exit();
    }
}
